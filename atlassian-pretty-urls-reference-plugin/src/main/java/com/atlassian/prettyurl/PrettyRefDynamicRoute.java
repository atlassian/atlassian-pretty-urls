package com.atlassian.prettyurl;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.prettyurls.api.route.DefaultUrlRouteRuleSetKey;
import com.atlassian.prettyurls.api.route.RouteService;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;

@Component
public class PrettyRefDynamicRoute implements InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(PrettyRefDynamicRoute.class);

    public static final DefaultUrlRouteRuleSetKey DYNAMIC_ROUTE_1 = new DefaultUrlRouteRuleSetKey("dynamicRoute1");
    private final RouteService routeService;

    @Autowired
    public PrettyRefDynamicRoute(final RouteService routeService) {
        this.routeService = routeService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.warn("Adding dynamic route /dynamically/are/{verb}");

        UrlRouteRuleSet dynamicRoute1 = new UrlRouteRuleSet.Builder()
                .setKey(DYNAMIC_ROUTE_1)
                .addTopLevelPath("/prettyurls")
                .addRule(
                        "/prettyurls/dynamically/are/{verb}",
                        "/plugins/servlet/PrettyActionUrlsActionHorribleName.jspa")
                .build();
        routeService.registerRoutes(dynamicRoute1);
    }

    @Override
    public void destroy() throws Exception {
        log.warn("Removing dynamic route /dynamically/are/{verb}");
        routeService.unregisterRoutes(DYNAMIC_ROUTE_1);
    }
}
