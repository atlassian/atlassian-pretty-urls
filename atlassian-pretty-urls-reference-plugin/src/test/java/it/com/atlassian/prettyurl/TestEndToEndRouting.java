package it.com.atlassian.prettyurl;

import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.integrationtesting.runner.TestGroups;

import static org.junit.Assert.assertEquals;

@RunWith(TestGroupRunner.class)
public class TestEndToEndRouting {

    private static final String BASEURL = System.getProperty("baseurl", "http://localhost:5990/refapp");
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String HORRIBLE_URL =
            System.getProperty("context.path", "/refapp") + "/plugins/servlet/PrettyActionUrlsActionHorribleName.jspa";
    private RestClient client;

    @Before
    public void setUp() {
        client = new RestClient();
    }

    @Test
    public void testEndToEndRouting_200() {

        assert200("/prettyurls/are/cool", HORRIBLE_URL, GET);
        assert200("/prettyurls/are/tops", HORRIBLE_URL, GET);
        assert200("/prettyurls/are/unreal", HORRIBLE_URL, GET);

        assert200("/prettyurls/http/get", HORRIBLE_URL, GET);
        assert200("/prettyurls/http/post", HORRIBLE_URL, POST);
        assert200("/prettyurls/http/sugar/get", HORRIBLE_URL, GET);
        assert200("/prettyurls/http/sugar/post", HORRIBLE_URL, POST);
    }

    @Test
    @TestGroups(
            excludes = {"confluence"},
            reason = "Confluence don't support 404 without login")
    public void testEndToEndRouting_404() {

        assert404("/prettyurls/http/get", POST);
        assert404("/prettyurls/http/post", GET);

        assert404("/prettyurls/http/sugar/get", POST);
        assert404("/prettyurls/http/sugar/post", GET);

        assert404("/prettyurls/mismatch/url", GET);
        assert404("/prettyurls/are/not/further/on", GET);
    }

    @Test
    public void testConditionalRoutes_200() {
        assert200("/conditional/on", HORRIBLE_URL, GET);
    }

    @Test
    @TestGroups(
            excludes = {"confluence"},
            reason = "Confluence don't support 404 without login")
    public void testConditionalRoutes_404() {
        assert404("/conditional/off", GET);
        assert404("/conditional/never", GET);

        // exception handling
        assert404("/exceptional/construction", GET);
        assert404("/exceptional/execution", GET);
    }

    @Test
    public void directoryTraversalIsHandledSafely() {
        // should be normalised to /prettyurls/are/cool
        assert200("/prettyurls/are/evil/../cool", HORRIBLE_URL, GET);
    }

    @Test
    @TestGroups(
            excludes = {"confluence"},
            reason = "Confluence don't support 404 without login")
    public void unableToDoDirectoryTraversalToProtectedResources() {
        // Uses the defined route <route from="/evil/{verb}" to="/secure/{verb}"/>
        assert404("/prettyurls/evil/../WEB-INF/web.xml", GET);
    }

    private void assert404(String url, String method) {
        getContent(url, 404, method);
    }

    private void assert200(String resourceUrl, String redirectURL, final String method) {
        String content = getContent(resourceUrl, 200, method);

        Document document = Jsoup.parse(content);
        String currentURI = document.select("#actionURI").text();
        assertEquals(redirectURL, currentURI);

        String actionMethod = document.select("#actionMethod").text();
        assertEquals(method.toUpperCase(), actionMethod);
    }

    private String getContent(String resourceURL, final int expectedSC, final String method) {
        String url = BASEURL + resourceURL;
        Resource resource = client.resource(url);

        ClientResponse response;
        if (POST.equalsIgnoreCase(method)) {
            response = resource.post(null);
        } else {
            response = resource.get();
        }

        assertEquals(expectedSC, response.getStatusCode());

        return response.getEntity(String.class);
    }
}
