# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [3.0.0 Unreleased]

### Changed
* Adds support for Java 11
* Now depends on Platform 5 being provided by the host application
* Updated AMPS version to 8.0 (the first version with Java 11 support) - Due to this the minimum Maven version
  required is now 3.5
* Updated platform version to 6.0.4(BSP-4171)