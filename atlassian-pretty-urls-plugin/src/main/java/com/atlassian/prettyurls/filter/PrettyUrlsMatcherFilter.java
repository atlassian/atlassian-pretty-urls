package com.atlassian.prettyurls.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.prettyurls.internal.route.UrlRouter;

/**
 * A filter that can DECIDE to map from pretty urls back to the other urls.
 * <p>
 * Splitting out the "matching and the redirecting" into 2 filters allows is to also "decorate" the resultant
 * request, which at the time of writing was missing from JIRA at least.  At the time of writing UNLESS you went through
 * a specific url top level path like /secure, then you never got the SiteMesh decoration filter applied to you.
 * <p>
 * This allows us to re-route AND decorate.
 */
@UnrestrictedAccess
public class PrettyUrlsMatcherFilter extends PrettyUrlsCommonFilter {

    private static final Logger log = LoggerFactory.getLogger(PrettyUrlsMatcherFilter.class);

    private final UrlRouter urlRouter;

    public PrettyUrlsMatcherFilter(UrlRouter urlRouter) {
        this.urlRouter = urlRouter;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        final String fromURI = httpServletRequest.getRequestURI();
        final UrlRouter.Result result = urlRouter.route(httpServletRequest, location);
        if (result.isRouted()) {
            String toURI = result.toURI();
            if (log.isDebugEnabled()) {
                log.debug("Will route from {} ==> {}", fromURI, toURI);
            }

            setInformationRequestVariables(httpServletRequest, fromURI, toURI);

            // this tells the dispatcher to act
            httpServletRequest.setAttribute(PRETTY_URLS_PERFORM_ROUTE, Boolean.TRUE);
        }
        // and now onto the dispatcher and possible decoration
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
