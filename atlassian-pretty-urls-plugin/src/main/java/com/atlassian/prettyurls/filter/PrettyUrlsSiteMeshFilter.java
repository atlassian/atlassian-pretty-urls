package com.atlassian.prettyurls.filter;

import java.io.IOException;
import java.util.List;
import javax.annotation.Nullable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.prettyurls.internal.util.UrlUtils;
import com.atlassian.prettyurls.module.SiteMeshModuleDescriptor;

/**
 * A filter that performs SiteMesh filtering IF the action has been routed and its asked for SiteMesh to be performed on it
 */
@UnrestrictedAccess
public class PrettyUrlsSiteMeshFilter extends PrettyUrlsCommonFilter {
    private static final Logger log = LoggerFactory.getLogger(PrettyUrlsSiteMeshFilter.class);

    private final Filter siteMeshFilterDelegate;
    private final PluginAccessor pluginAccessor;

    public PrettyUrlsSiteMeshFilter(@ComponentImport final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
        this.siteMeshFilterDelegate = loadFilter("com.opensymphony.sitemesh.webapp.SiteMeshFilter");
    }

    /**
     * Some applications such as Stash don't have SiteMesh so we test the waters first to see if it's around,
     * if NOT we become a Noop filter
     */
    @Nullable
    static Filter loadFilter(String filterClassName) {
        try {
            return Class.forName(filterClassName)
                    .asSubclass(Filter.class)
                    .getConstructor()
                    .newInstance();
        } catch (ClassNotFoundException e) {
            log.debug("Class [{}] is not found", filterClassName);
            return null;
        } catch (ClassCastException e) {
            log.warn("Class [{}] is not of type [{}]", filterClassName, Filter.class.getName());
            return null;
        } catch (ReflectiveOperationException e) {
            log.warn("Failed to instantiate filter of class [{}]", filterClassName);
            return null;
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        if (siteMeshFilterDelegate != null) {
            siteMeshFilterDelegate.init(filterConfig);
        }
    }

    @Override
    public void destroy() {
        if (siteMeshFilterDelegate != null) {
            siteMeshFilterDelegate.destroy();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        if (siteMeshFilterDelegate == null) {
            //
            // Since there is no SiteMesh we are a no-op filter
            //
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpServletRequest httpServletRequest = preventDoubleInvocation(servletRequest, servletResponse, filterChain);
        if (httpServletRequest == null) {
            return;
        }

        if (needsSiteMeshDecoration(httpServletRequest)) {
            //
            // we need to cause decoration to happen for this request
            //
            siteMeshFilterDelegate.doFilter(servletRequest, servletResponse, filterChain);
        } else {
            //
            // we are not to be decorated so again we become a no-op filter and pass straight through to the rest of the
            // chain
            //
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private boolean needsSiteMeshDecoration(HttpServletRequest httpServletRequest) {
        // if SiteMesh has already run on this request then don't bother
        if (httpServletRequest.getAttribute("com.opensymphony.sitemesh.APPLIED_ONCE") != null) {
            return false;
        }

        String requestURI = makeRequestURI(httpServletRequest);
        List<SiteMeshModuleDescriptor> siteMeshModules =
                pluginAccessor.getEnabledModuleDescriptorsByClass(SiteMeshModuleDescriptor.class);
        for (SiteMeshModuleDescriptor module : siteMeshModules) {
            if (requestURI.startsWith(module.getPath())) {
                return true;
            }
        }
        return false;
    }

    private String makeRequestURI(HttpServletRequest httpServletRequest) {
        String requestURI = httpServletRequest.getRequestURI();
        String context = httpServletRequest.getContextPath();
        if (requestURI.startsWith(context)) {
            requestURI = requestURI.substring(context.length());
        }
        return UrlUtils.startWithSlash(requestURI);
    }
}
