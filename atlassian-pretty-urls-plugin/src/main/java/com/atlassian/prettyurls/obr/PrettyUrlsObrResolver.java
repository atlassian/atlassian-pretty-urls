package com.atlassian.prettyurls.obr;

/**
 * Initially it was thought that this was needed to get OBR packaging to work but turns out
 * its not needed and you can specify RequiresBundle to cause dependencies on plugins to which
 * you have no compile time dependency.
 *
 * @deprecated since 1.11
 */
@Deprecated
public class PrettyUrlsObrResolver {
    private PrettyUrlsObrResolver() {}

    public static void resolved() {}
}
