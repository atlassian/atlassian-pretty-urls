package com.atlassian.prettyurls.internal.util;

import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;

public class LogUtils {

    private LogUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Logs an Exception event at ERROR level and DEBUG level (if enabled) using the Logger provided,
     * with the best effort to produce a meaningful log entry, but not failing if any or all
     * parameters provided are null.
     * @param logger The logger to be used for logging the Exception event
     * @param exception The Exception to be added to the log
     * @param message The custom message to be added to the log
     */
    public static void logExceptionEvent(final Logger logger, final Throwable exception, final String message) {
        Optional<Logger> optionalLogger = Optional.ofNullable(logger);

        optionalLogger.ifPresent(
                log -> log.error(String.valueOf(message).concat(" ").concat(String.valueOf(exception))));

        optionalLogger
                .filter(Objects::nonNull)
                .filter(Logger::isDebugEnabled)
                .ifPresent(log -> log.debug(String.valueOf(message), exception));
    }
}
