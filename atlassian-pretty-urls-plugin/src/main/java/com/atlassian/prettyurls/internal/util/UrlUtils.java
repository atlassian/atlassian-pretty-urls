package com.atlassian.prettyurls.internal.util;

import org.glassfish.jersey.uri.UriTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * URL handling
 */
public class UrlUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(UrlUtils.class);

    public static String startWithSlash(String uri) {
        return uri.startsWith("/") ? uri : "/" + uri;
    }

    public static String removePrecedingSlash(String uri) {
        return uri.startsWith("/") ? uri.substring(1) : uri;
    }

    public static String removeTrailingSlash(String uri) {
        return uri.endsWith("/") ? uri.substring(0, uri.length() - 1) : uri;
    }

    public static String prependPath(String path, String uriStr) {
        path = removeTrailingSlash(startWithSlash(path));
        uriStr = removePrecedingSlash(uriStr);
        return path + "/" + uriStr;
    }

    public static boolean validateUri(String uriStr) {
        try {
            new UriTemplate(uriStr);
            return true;
        } catch (IllegalArgumentException e) {
            LOGGER.error("Unable to parse routing URI {}", uriStr);
            return false;
        }
    }
}
