package com.atlassian.prettyurls.internal.route;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.ParametersAreNonnullByDefault;

import org.glassfish.jersey.uri.UriTemplate;
import io.atlassian.fugue.Option;

import com.atlassian.prettyurls.api.route.UrlRouteRule;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;

/**
 * This will match the request URI to the list of route rules to find the most appropriate one
 */
@ParametersAreNonnullByDefault
class UrlMatcher {

    /**
     * If we have multiple {@link com.atlassian.prettyurls.api.route.UrlRouteRuleSet}s that match the top level
     * path then we need to use a different strategy on how we match.  The matcher needs to be told this as input.
     */
    enum Strategy {
        /**
         * With multiple rule sets as input we use the JAX-RS strategy
         *
         * See https://jsr311.java.net//nonav/releases/1.0/spec/spec3.html#x3-350003.7.2
         *
         * - Section 3.7.2 - Request Matching
         *
         * "Sort E using the number of literal characters in each member as the primary
         * key (descending order), the number of capturing groups as a secondary key (descending order)
         * and the number of capturing groups with non-default regular expressions (i.e. not ‘([^  /]+?)’)
         * as the tertiary key (descending order).
         *
         * This is done for us by UriTemplate itself via {@link UriTemplate#COMPARATOR}
         */
        JAX_RS_MATCHING {
            @Override
            List<UrlRouteRule> sortRules(List<UrlRouteRule> routeRules) {
                return routeRules.stream()
                        .sorted(Comparator.comparing(
                                urlRouteRule -> new UriTemplate(urlRouteRule.getFrom()), UriTemplate.COMPARATOR))
                        .collect(Collectors.toCollection(ArrayList::new));
            }
        },
        /**
         * With a single rule set as input, we use the legacy "list order" strategy.  Since there are many usages
         * of pretty urls out in the wild, we can't move to a JAX-RS strategy only since it might break things.
         */
        LIST_ORDER_MATCHING {
            @Override
            List<UrlRouteRule> sortRules(List<UrlRouteRule> routeRules) {
                return routeRules;
            }
        };

        /**
         * Sort the list of route rules according to each strategy
         *
         * @param routeRules the route rules to sort
         *
         * @return a list of sorted route rules
         */
        abstract List<UrlRouteRule> sortRules(List<UrlRouteRule> routeRules);
    }

    static class Result {
        private final Map<String, String> parsedVariableValues;
        private final Option<UrlRouteRule> matchingRule;

        public Result(Option<UrlRouteRule> matchingRule, Map<String, String> parsedVariableValues) {
            this.parsedVariableValues = parsedVariableValues;
            this.matchingRule = matchingRule;
        }

        public Option<UrlRouteRule> getMatchingRule() {
            return matchingRule;
        }

        public Map<String, String> getParsedVariableValues() {
            return parsedVariableValues;
        }

        public boolean matches() {
            return matchingRule.isDefined();
        }
    }

    /**
     * This will match a list of route rules against request URI.  The best rule wins.  Just how we determine the best rule
     * is heuristically driven.  If there is only a single {@link com.atlassian.prettyurls.api.route.UrlRouteRuleSet} in play then
     * it is simply the first rule in the list that matches.
     *
     * However if there are multiple rule sets in play then we use a different strategy.  We pick the most specific rule based on
     * how specific the {@link UriTemplate} is.
     *
     * @param requestURI the request URI in play
     * @param routeRules the list of possible matching rules
     * @param strategy   the number of top level matching rule sets that the rules came from
     *
     * @return a match result
     */
    Result getMatchingRule(String requestURI, List<UrlRouteRule> routeRules, Strategy strategy) {

        final List<UrlRouteRule> sortedRules = strategy.sortRules(routeRules);

        final Map<String, String> parsedVariableValues = new HashMap<>();
        // now match - first rule to match is what we return
        for (UrlRouteRule routeRule : sortedRules) {
            UriTemplate uriTemplate = new UriTemplate(routeRule.getFrom());
            if (uriTemplate.match(requestURI, parsedVariableValues)) {
                return new Result(some(routeRule), parsedVariableValues);
            }
        }
        return new Result(none(), parsedVariableValues);
    }
}
