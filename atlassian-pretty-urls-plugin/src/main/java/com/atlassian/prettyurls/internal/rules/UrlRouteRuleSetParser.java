package com.atlassian.prettyurls.internal.rules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.prettyurls.api.route.DefaultUrlRouteRuleSetKey;
import com.atlassian.prettyurls.api.route.RoutePredicate;
import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.atlassian.prettyurls.internal.util.UrlUtils;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

import static com.atlassian.prettyurls.api.route.UrlRouteRule.ParameterMode.PASS_UNMAPPED;
import static com.atlassian.prettyurls.internal.util.UrlUtils.validateUri;

/**
 * Parses out the {@link com.atlassian.prettyurls.api.route.UrlRouteRuleSet} for a given plugin from its module
 * descriptor XML
 */
public class UrlRouteRuleSetParser {
    private static final Logger log = LoggerFactory.getLogger(UrlRouteRuleSetParser.class);
    private static final List<String> HEAD = singletonList("HEAD");
    private static final List<String> GET = singletonList("GET");
    private static final List<String> POST = singletonList("POST");
    private static final List<String> PUT = singletonList("PUT");
    private static final List<String> DELETE = singletonList("DELETE");
    private static final List<String> OPTIONS = singletonList("OPTIONS");
    private static final List<String> PATCH = singletonList("PATCH");

    /**
     * This maps between the underlying plugin system element conditions
     */
    public interface PredicateMaker {
        RoutePredicate<UrlRouteRuleSet> makeRuleSetPredicate(Element routing);

        RoutePredicate<UrlRouteRule> makeRulePredicate(Element route);
    }

    public UrlRouteRuleSet parse(
            String moduleKey, Element element, final FilterLocation location, PredicateMaker predicateMaker) {
        requireNonNull(moduleKey);
        requireNonNull(element);

        UrlRouteRuleSet.Builder builder = new UrlRouteRuleSet.Builder()
                .setKey(new DefaultUrlRouteRuleSetKey(moduleKey))
                .setLocation(location);

        String path = element.attributeValue("path", "").trim();
        if (!path.isEmpty()) {
            if (validatePath(path)) {
                builder.addTopLevelPath(UrlUtils.startWithSlash(path));
            } else {
                log.error("'{}' is not an acceptable top level path for URL routing.", path);
                return null;
            }
        } else {
            log.error("You must provide a path attribute in order to get URL routing.");
            return null;
        }

        builder.setPredicate(predicateMaker.makeRuleSetPredicate(element));

        // standard route instruction
        parseRules("route", element, builder, predicateMaker, path, Collections.emptyList());

        // the following are syntactic sugar eg.
        //
        //    <get to='' from=''/> is the same as
        //    <route to='' from='' verbs='get' />
        //
        parseRules("head", element, builder, predicateMaker, path, HEAD);
        parseRules("get", element, builder, predicateMaker, path, GET);
        parseRules("post", element, builder, predicateMaker, path, POST);
        parseRules("put", element, builder, predicateMaker, path, PUT);
        parseRules("delete", element, builder, predicateMaker, path, DELETE);
        parseRules("options", element, builder, predicateMaker, path, OPTIONS);
        parseRules("patch", element, builder, predicateMaker, path, PATCH);

        return builder.build();
    }

    private void parseRules(
            String elementName,
            Element element,
            UrlRouteRuleSet.Builder builder,
            final PredicateMaker predicateMaker,
            String path,
            List<String> providedHttpVerbs) {
        //noinspection unchecked
        List<Element> elements = element.elements(elementName);
        for (Element e : elements) {
            String fromStr = e.attributeValue("from");
            String toStr = e.attributeValue("to", "").trim();
            RoutePredicate<UrlRouteRule> routePredicate = predicateMaker.makeRulePredicate(e);

            if (toStr.isEmpty()) {
                log.error("Encountered blank to=\"\" rule.  Ignoring it...");
                continue;
            }
            if (fromStr == null) {
                log.error("Missing from=\"\" rule.  Ignoring it...");
                continue;
            }
            String from;
            if (fromStr.trim().isEmpty()) {
                // ok this is the case where they want to map directly from the top level path
                from = path;
            } else {
                from = UrlUtils.prependPath(path, fromStr);
            }

            // to destination does not get the prepended top level path
            String to = UrlUtils.prependPath("", toStr);

            List<String> httpVerbs = providedHttpVerbs;
            if (httpVerbs.isEmpty()) {
                httpVerbs = parseHttpVerbs(e);
            }

            if (validateUri(from) && validateUri(to)) {
                builder.addRule(from, to, httpVerbs, routePredicate, PASS_UNMAPPED);
            }
        }
    }

    private List<String> parseHttpVerbs(Element e) {
        ArrayList<String> httpVerbs = new ArrayList<>();
        String verbs = e.attributeValue("verbs", "");
        String[] split = verbs.split(",");
        for (String verb : split) {
            verb = verb.trim();
            if (!verb.isEmpty()) {
                httpVerbs.add(verb.toUpperCase());
            }
        }
        return httpVerbs;
    }

    private boolean validatePath(String path) {
        return !path.equals("/");
    }
}
