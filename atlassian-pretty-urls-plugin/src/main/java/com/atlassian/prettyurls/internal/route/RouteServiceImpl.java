package com.atlassian.prettyurls.internal.route;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.prettyurls.api.route.RouteService;
import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSetKey;
import com.atlassian.prettyurls.module.UrlRouteModuleDescriptor;

import static java.util.Objects.requireNonNull;

import static com.atlassian.prettyurls.internal.util.UrlUtils.startWithSlash;
import static com.atlassian.prettyurls.internal.util.UrlUtils.validateUri;

/**
 */
@ExportAsService
@Component
public class RouteServiceImpl implements RouteService, InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(RouteServiceImpl.class);

    private final PluginAccessor pluginAccessor;
    private final PluginEventManager pluginEventManager;
    private final Map<UrlRouteRuleSetKey, UrlRouteRuleSet> dynamicallyRegistered;

    private volatile PluginModuleTracker<Object, UrlRouteModuleDescriptor> moduleTracker;

    @Autowired
    public RouteServiceImpl(
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final PluginEventManager pluginEventManager) {
        this.pluginAccessor = pluginAccessor;
        this.pluginEventManager = pluginEventManager;
        this.dynamicallyRegistered = new ConcurrentHashMap<>();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // JIRA does not have the caching module descriptor factory and hence getEnabledModuleDescriptorsByClass is
        // really slow
        // There is a 30% penalty on URL because this module code gets called on every JIRA request.  So we use the
        // plugin tracker directly
        // as a way to avoid this for now.  Once JIRA gets the cached pluginAccessor then we can remove his code
        //    -  BB 2013  (<-- ha ha - he said remove code in the future ...lol)
        moduleTracker =
                new DefaultPluginModuleTracker<>(pluginAccessor, pluginEventManager, UrlRouteModuleDescriptor.class);
    }

    @Override
    public void destroy() throws Exception {
        moduleTracker.close();
    }

    @Override
    public Set<UrlRouteRuleSet> getRoutes() {
        Set<UrlRouteRuleSet> urlRouteRuleSets = new LinkedHashSet<>();
        for (UrlRouteModuleDescriptor md : moduleTracker.getModuleDescriptors()) {
            try {
                UrlRouteRuleSet ruleSet = md.getRuleSet();
                urlRouteRuleSets.add(ruleSet);
            } catch (Exception ignore) {
            }
        }
        urlRouteRuleSets.addAll(dynamicallyRegistered.values());
        return urlRouteRuleSets;
    }

    public Set<UrlRouteRuleSet> getRouteRuleSets(FilterLocation filterLocation, String requestURI) {
        Set<UrlRouteRuleSet> urlRouteRuleSets = new LinkedHashSet<>();
        for (UrlRouteModuleDescriptor md : moduleTracker.getModuleDescriptors()) {
            UrlRouteRuleSet ruleSet = safelyUse(requestURI, filterLocation, md.getRuleSet());
            if (ruleSet != null) {
                urlRouteRuleSets.add(ruleSet);
            }
        }
        for (UrlRouteRuleSet ruleSet : dynamicallyRegistered.values()) {
            ruleSet = safelyUse(requestURI, filterLocation, ruleSet);
            if (ruleSet != null) {
                urlRouteRuleSets.add(ruleSet);
            }
        }
        return urlRouteRuleSets;
    }

    private UrlRouteRuleSet safelyUse(String requestURI, FilterLocation filterLocation, UrlRouteRuleSet routeRuleSet) {
        if (routeRuleSet != null) {
            try {
                if (filterLocation.equals(routeRuleSet.getFilterLocation())) {
                    // if the ruleset top level path doesn't match the current request then don't return it
                    if (matchesTopLevelPath(routeRuleSet, requestURI)) {
                        return routeRuleSet;
                    }
                }
            } catch (RuntimeException e) {
                //
                // We do this so that ANY race conditions around accessing the module descriptor and its enablement or
                // not
                // means that the request is NOT stopped but rather a route opportunity is skipped.  This may result in
                // a 404
                // or it may not but it wont be a 500 because of a bad module descriptor
                //
                log.debug("Unable to use UrlRouteModuleDescriptor.  Ignoring...");
            }
        }
        return null;
    }

    private boolean matchesTopLevelPath(UrlRouteRuleSet urlRouteRuleSet, String requestURI) {
        for (String path : urlRouteRuleSet.getTopLevelPaths()) {
            if (requestURI.startsWith(startWithSlash(path))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void registerRoutes(final UrlRouteRuleSet urlRouteRuleSet) {
        requireNonNull(urlRouteRuleSet);
        validateRoutes(urlRouteRuleSet);
        dynamicallyRegistered.put(requireNonNull(urlRouteRuleSet.getKey()), urlRouteRuleSet);
    }

    @Override
    public UrlRouteRuleSet unregisterRoutes(final UrlRouteRuleSetKey key) {
        return dynamicallyRegistered.remove(requireNonNull(key));
    }

    private void validateRoutes(UrlRouteRuleSet urlRouteRuleSet) {
        for (UrlRouteRule urlRouteRule : urlRouteRuleSet.getUrlRouteRules()) {
            if (!validateUri(urlRouteRule.getFrom())) {
                throw new IllegalArgumentException(
                        "Unable to parse routing URI 'from' %s".formatted(urlRouteRule.getFrom()));
            }
            if (!validateUri(urlRouteRule.getTo())) {
                throw new IllegalArgumentException(
                        "Unable to parse routing URI 'to' %s".formatted(urlRouteRule.getTo()));
            }
        }
    }
}
