package com.atlassian.prettyurls.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.junit.Test;
import com.opensymphony.sitemesh.webapp.SiteMeshFilter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;

public class PrettyUrlsSiteMeshFilterTest {
    @Test
    public void loadSiteMeshFilter() {
        var filter = PrettyUrlsSiteMeshFilter.loadFilter(SiteMeshFilter.class.getName());
        assertThat(filter, instanceOf(SiteMeshFilter.class));
    }

    @Test
    public void nonExistentFilter() {
        var filter = PrettyUrlsSiteMeshFilter.loadFilter("foo.bar");
        assertThat(filter, nullValue());
    }

    @Test
    public void filterOfWrongType() {
        var filter = PrettyUrlsSiteMeshFilter.loadFilter(getClass().getName());
        assertThat(filter, nullValue());
    }

    @Test
    public void filterCannotBeCreated() {
        var filter = PrettyUrlsSiteMeshFilter.loadFilter(NonInstantiatableFilter.class.getName());
        assertThat(filter, nullValue());
    }

    private static final class NonInstantiatableFilter implements Filter {
        private NonInstantiatableFilter() {}

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {}
    }
}
