package com.atlassian.prettyurls.module;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.atlassian.prettyurls.internal.route.MemoryAppender;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(JUnitParamsRunner.class)
public class UrlRouteModuleDescriptorTest {

    @Mock
    private ModuleFactory moduleFactory;

    @Mock
    private HostContainer hostContainer;

    @Mock
    private Plugin plugin;

    @Mock
    private Dom4jDelegatingElement element;

    @Mock
    private org.dom4j.Element dom4jElement;

    @Mock
    private Dom4jDelegatingElement elementCondition;

    @Mock
    private Condition condition;

    @Mock
    private HttpServletRequest request;

    @Mock
    private UrlRouteRuleSet ruleSet;

    private List<Element> elements;
    private static MemoryAppender memoryAppender;
    private UrlRouteModuleDescriptor urlRouteModuleDescriptor;
    private Logger logger;
    private static final String LOGGER_NAME = "com.atlassian.prettyurls.module";
    private static final String ERROR_MSG_MAKING_CONDITION = "Error while making condition.";
    private static final String ERROR_MSG_AUTOWIRING_CONDITION = "Error while autowiring condition class.";
    private static final String ERROR_MSG_RUNNING_CONDITION = "Error while running condition.";

    @Before
    public void setup() {
        initMocks(this);
        when(element.getDelegate()).thenReturn(dom4jElement);
        this.elements = Arrays.asList(elementCondition, elementCondition);
        when(elementCondition.getDelegate()).thenReturn(mock(org.dom4j.Element.class));
        urlRouteModuleDescriptor = new UrlRouteModuleDescriptor(moduleFactory, hostContainer);
        when(element.attributeValue(anyString(), anyString())).thenReturn("before-dispatch");
        when(element.attributeValue("key")).thenReturn("key");
        setupLogging();
    }

    @After
    public void after() {
        memoryAppender.reset();
        memoryAppender.stop();
    }

    private void setupLogging() {
        logger = (Logger) LoggerFactory.getLogger(LOGGER_NAME);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
    }

    private void startLogging(Level level) {
        logger.setLevel(level);
        logger.addAppender(memoryAppender);
        memoryAppender.start();
    }

    @Test
    @Parameters(method = "getParameters")
    public void testExceptionWhileMakingCondition(Level level, int expectedError, int expectedDebug) {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenThrow(new PluginParseException());

        startLogging(level);
        urlRouteModuleDescriptor.enabled();

        assertEquals(
                memoryAppender.search(ERROR_MSG_MAKING_CONDITION, Level.ERROR).size(), expectedError);
        assertEquals(
                memoryAppender
                        .search(PluginParseException.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError);
        assertEquals(
                memoryAppender.search(ERROR_MSG_MAKING_CONDITION, Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    @Test
    @Parameters(method = "getParameters")
    public void testLinkageErrorWhileMakingCondition(Level level, int expectedError, int expectedDebug) {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenThrow(new LinkageError());

        startLogging(level);
        urlRouteModuleDescriptor.enabled();

        assertEquals(
                memoryAppender.search(ERROR_MSG_MAKING_CONDITION, Level.ERROR).size(), expectedError);
        assertEquals(
                memoryAppender
                        .search(LinkageError.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError);
        assertEquals(
                memoryAppender.search(ERROR_MSG_MAKING_CONDITION, Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    @Test(expected = OutOfMemoryError.class)
    public void testThrowableWhileMakingCondition() {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenThrow(new OutOfMemoryError());

        urlRouteModuleDescriptor.enabled();
    }

    @Test
    @Parameters(method = "getParameters")
    public void testExceptionWhileAutowiringCondition(Level level, int expectedError, int expectedDebug) {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenThrow(new IllegalArgumentException());

        startLogging(level);
        urlRouteModuleDescriptor.enabled();

        // Due to the mocking used, the list of elements need at least 2 elements
        // Each element in the list produce a log entry in this test, thus, they must be accounted for assertion
        assertEquals(
                memoryAppender
                        .search(ERROR_MSG_AUTOWIRING_CONDITION, Level.ERROR)
                        .size(),
                expectedError * elements.size());
        assertEquals(
                memoryAppender
                        .search(IllegalArgumentException.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError * elements.size());
        assertEquals(
                memoryAppender
                        .search(ERROR_MSG_AUTOWIRING_CONDITION, Level.DEBUG)
                        .size(),
                expectedDebug * elements.size());
        assertEquals(
                memoryAppender.countEventsForLogger(LOGGER_NAME), (expectedError + expectedDebug) * elements.size());
    }

    @Test
    @Parameters(method = "getParameters")
    public void testLinkageErrorWhileAutowiringCondition(Level level, int expectedError, int expectedDebug) {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenThrow(new LinkageError());

        startLogging(level);
        urlRouteModuleDescriptor.enabled();

        // Due to the mocking used, the list of elements need at least 2 elements
        // Each element in the list produce a log entry in this test, thus, they must be accounted for assertion
        assertEquals(
                memoryAppender
                        .search(ERROR_MSG_AUTOWIRING_CONDITION, Level.ERROR)
                        .size(),
                expectedError * elements.size());
        assertEquals(
                memoryAppender
                        .search(LinkageError.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError * elements.size());
        assertEquals(
                memoryAppender
                        .search(ERROR_MSG_AUTOWIRING_CONDITION, Level.DEBUG)
                        .size(),
                expectedDebug * elements.size());
        assertEquals(
                memoryAppender.countEventsForLogger(LOGGER_NAME), (expectedError + expectedDebug) * elements.size());
    }

    @Test(expected = OutOfMemoryError.class)
    public void testThrowableWhileAutowiringCondition() {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenThrow(new OutOfMemoryError());

        urlRouteModuleDescriptor.enabled();
    }

    @Test
    public void testRunningCondition() {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenReturn(condition);
        when(condition.shouldDisplay(any())).thenReturn(true);

        urlRouteModuleDescriptor.enabled();
        Boolean result = urlRouteModuleDescriptor.getRuleSet().getPredicate().apply(request, ruleSet);

        assertTrue(result);
    }

    @Test
    @Parameters(method = "getParameters")
    public void testExceptionWhileRunningCondition(Level level, int expectedError, int expectedDebug) {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenReturn(condition);
        when(condition.shouldDisplay(any())).thenThrow(new PluginParseException());

        startLogging(level);
        urlRouteModuleDescriptor.enabled();
        urlRouteModuleDescriptor.getRuleSet().getPredicate().apply(request, ruleSet);

        assertEquals(
                memoryAppender.search(ERROR_MSG_RUNNING_CONDITION, Level.ERROR).size(), expectedError);
        assertEquals(
                memoryAppender
                        .search(PluginParseException.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError);
        assertEquals(
                memoryAppender.search(ERROR_MSG_RUNNING_CONDITION, Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    @Test
    @Parameters(method = "getParameters")
    public void testLinkageErrorWhileRunningCondition(Level level, int expectedError, int expectedDebug) {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenReturn(condition);
        when(condition.shouldDisplay(any())).thenThrow(new LinkageError());

        startLogging(level);
        urlRouteModuleDescriptor.enabled();
        urlRouteModuleDescriptor.getRuleSet().getPredicate().apply(request, ruleSet);

        assertEquals(
                memoryAppender.search(ERROR_MSG_RUNNING_CONDITION, Level.ERROR).size(), expectedError);
        assertEquals(
                memoryAppender
                        .search(LinkageError.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError);
        assertEquals(
                memoryAppender.search(ERROR_MSG_RUNNING_CONDITION, Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    @Test(expected = OutOfMemoryError.class)
    public void testThrowableWhileRunningCondition() {
        urlRouteModuleDescriptor.init(plugin, element);
        when(element.elements("condition")).thenReturn(elements);
        when(elementCondition.attributeValue("class")).thenReturn("conditionClassName");
        when(hostContainer.create(null)).thenReturn(condition);
        when(condition.shouldDisplay(any())).thenThrow(new OutOfMemoryError());

        urlRouteModuleDescriptor.enabled();
        urlRouteModuleDescriptor.getRuleSet().getPredicate().apply(request, ruleSet);
    }

    private Iterable getParameters() {
        return Arrays.asList(new Object[][] {
            // level, expectedError, expectedDebug
            {Level.ERROR, 1, 0},
            {Level.DEBUG, 1, 1}
        });
    }
}
