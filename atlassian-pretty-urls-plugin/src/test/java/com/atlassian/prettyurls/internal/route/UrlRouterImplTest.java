package com.atlassian.prettyurls.internal.route;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.prettyurls.api.route.DefaultUrlRouteRuleSetKey;
import com.atlassian.prettyurls.api.route.RoutePredicate;
import com.atlassian.prettyurls.api.route.RoutePredicates;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.atlassian.prettyurls.mocks.MockHttpServletRequest;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.servlet.filter.FilterLocation.BEFORE_DISPATCH;

@RunWith(JUnitParamsRunner.class)
public class UrlRouterImplTest {
    private UrlRouteRuleSet routeRuleSet;
    private UrlRouterImpl router;
    private MockHttpServletRequest request;
    private UrlRouteRuleSet.Builder builder;
    private static MemoryAppender memoryAppender;
    private Logger logger;
    private static final String LOGGER_NAME = "com.atlassian.prettyurls.internal.route";
    private static final String ERROR_MSG_PREDICATE = "Error while running predicate.";

    @Before
    public void setUp() throws Exception {
        builder = newRouteSetBuilder();

        router = new UrlRouterImpl(null) {
            Set<UrlRouteRuleSet> getRouteRuleSets(FilterLocation filterLocation, String requestURI) {
                return singleton(routeRuleSet);
            }
        };

        setupLogging();
    }

    @After
    public void cleanUp() {
        memoryAppender.reset();
        memoryAppender.stop();
    }

    private UrlRouteRuleSet.Builder newRouteSetBuilder() {
        UrlRouteRuleSet.Builder builder = new UrlRouteRuleSet.Builder();
        builder.setKey(new DefaultUrlRouteRuleSetKey("test.module"));
        builder.addTopLevelPath("avenger");
        return builder;
    }

    private void setupLogging() {
        logger = (Logger) LoggerFactory.getLogger(LOGGER_NAME);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
    }

    private void startLogging(Level level) {
        logger.setLevel(level);
        logger.addAppender(memoryAppender);
        memoryAppender.start();
    }

    @Test
    public void testRoute() throws Exception {
        builder.addRule("/avenger/thor/{id}/{weapon}", "/secure/Thor.jspa");
        builder.addRule("/avenger/thor/{id}/{weapon}/hair{style}", "/secure/Thor!flickHair.jspa");

        builder.addRule("/avenger/hulk/{id}", "/secure/Hulk!rage.jspa");

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/thor/id123/weapon1");
        //
        // we no longer put the extra parameters into the redirect URL
        // as the requestDispatcher code does this for us
        //
        request.addParameter("heat", "high");

        UrlRouter.Result actual = router.route(request, BEFORE_DISPATCH);
        assertURLEquals("/secure/Thor.jspa?id=id123&weapon=weapon1", actual);

        request = new MockHttpServletRequest("/miss/thor/id123/weapon1");
        actual = router.route(request, BEFORE_DISPATCH);
        assertFalse(actual.isRouted());

        request = new MockHttpServletRequest("/avenger/thor/id123/weapon1/hairWAVY");
        request.addParameter("heat", "high");

        actual = router.route(request, BEFORE_DISPATCH);
        assertURLEquals("/secure/Thor!flickHair.jspa?id=id123&weapon=weapon1&style=WAVY&", actual);

        request = new MockHttpServletRequest("/avenger/hulk/greenHair");

        actual = router.route(request, BEFORE_DISPATCH);
        assertURLEquals("/secure/Hulk!rage.jspa?id=greenHair", actual);
    }

    @Test
    public void testRouteWithGeneratorFunction() throws Exception {
        builder.addRule(
                "/avenger/thor/{id}/{weapon}",
                (request, variables) -> "/avenger/thor/" + variables.get("id") + "/big_" + variables.get("weapon"),
                Collections.emptyList());

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/thor/id123/weapon1");
        UrlRouter.Result actual = router.route(request, BEFORE_DISPATCH);
        assertURLEquals("/avenger/thor/id123/big_weapon1", actual);
    }

    @Test
    public void testWithContext() throws Exception {
        builder.addRule("/avenger/thor/{id}/{weapon}", "/secure/Thor.jspa");

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/somecontext/avenger/thor/id123/weapon1");
        request.setContextPath("/somecontext");

        assertURLEquals("/secure/Thor.jspa?id=id123&weapon=weapon1", router.route(request, BEFORE_DISPATCH));
    }

    @Test
    public void testToURIMapping() throws Exception {
        builder.addRule("/avenger/{character}/{id}/{weapon}", "/secure/LookIts{character}.jspa");

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/Thor/id123/hammer");
        assertURLEquals("/secure/LookItsThor.jspa?id=id123&weapon=hammer", router.route(request, BEFORE_DISPATCH));

        request = new MockHttpServletRequest("/avenger/TheHulk/id456/rage");
        assertURLEquals("/secure/LookItsTheHulk.jspa?id=id456&weapon=rage", router.route(request, BEFORE_DISPATCH));
    }

    @Test
    public void testToURIMappingToRequestParams() throws Exception {
        builder.addRule("/avenger/{character}/{id}/{weapon}", "/secure/LookIts{character}.jspa?tool={weapon}");

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/Thor/id123/hammer");
        assertURLEquals("/secure/LookItsThor.jspa?tool=hammer&id=id123", router.route(request, BEFORE_DISPATCH));
    }

    @Test
    public void testHttpVerbs() throws Exception {
        builder.addRule("/avenger/Thor", "/secure/Thor.jspa", singletonList("GET"));

        builder.addRule("/avenger/Thor/POSTorGET", "/secure/Thor.jspa", asList("GET", "POST"));

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/Thor");
        request.setMethod("POST");

        UrlRouterImpl.Result result = router.route(request, BEFORE_DISPATCH);
        assertFalse(result.isRouted());

        request.setMethod("get");
        assertURLEquals("/secure/Thor.jspa", router.route(request, BEFORE_DISPATCH));

        request.setMethod("GET");
        assertURLEquals("/secure/Thor.jspa", router.route(request, BEFORE_DISPATCH));

        request = new MockHttpServletRequest("/avenger/Thor/POSTorGET");
        request.setMethod("DELETE");
        result = router.route(request, BEFORE_DISPATCH);
        assertFalse(result.isRouted());

        request.setMethod("GET");
        assertURLEquals("/secure/Thor.jspa", router.route(request, BEFORE_DISPATCH));

        request.setMethod("POST");
        assertURLEquals("/secure/Thor.jspa", router.route(request, BEFORE_DISPATCH));
    }

    @Test
    public void uriWithDirectoryTraversalIsNormalised() throws Exception {
        request = new MockHttpServletRequest("/avenger/Thor/../CaptainAmerica");
        request.setMethod("get");

        String requestUri = router.makeRequestURI(request);

        assertNotNull(requestUri);
        assertThat(requestUri, equalTo("/avenger/CaptainAmerica"));
    }

    @Test
    public void badUriReturnsUnRoutableRoute() {
        request = new MockHttpServletRequest("http://avenger/^Thor^");
        request.setMethod("get");

        UrlRouterImpl.Result route = router.route(request, BEFORE_DISPATCH);

        assertNotNull(route);
        assertFalse(route.isRouted());
        assertNull(route.toURI());
    }

    @Test
    public void testLowLevelRoutePredicates() throws Exception {
        builder.addRule("/avenger/Thor", "/secure/Thor.jspa", singletonList("GET"), RoutePredicates.alwaysFalse());

        builder.addRule(
                "/avenger/Thor/POSTorGET", "/secure/Thor.jspa", asList("GET", "POST"), RoutePredicates.alwaysTrue());

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/Thor");
        request.setMethod("GET");

        UrlRouterImpl.Result result = router.route(request, BEFORE_DISPATCH);
        assertFalse(result.isRouted());

        request = new MockHttpServletRequest("/avenger/Thor/POSTorGET");
        request.setMethod("GET");

        result = router.route(request, BEFORE_DISPATCH);
        assertTrue(result.isRouted());
    }

    @Test
    public void testTopLevelRoutePredicates_when_off() throws Exception {
        // turn the top level off

        builder.setPredicate(RoutePredicates.<UrlRouteRuleSet>alwaysFalse());

        builder.addRule("/avenger/Thor", "/secure/Thor.jspa", singletonList("GET"), RoutePredicates.alwaysTrue());

        builder.addRule(
                "/avenger/Thor/POSTorGET", "/secure/Thor.jspa", asList("GET", "POST"), RoutePredicates.alwaysTrue());

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/Thor");

        UrlRouterImpl.Result result = router.route(request, BEFORE_DISPATCH);
        assertFalse(result.isRouted());

        request = new MockHttpServletRequest("/avenger/Thor/POSTorGET");

        result = router.route(request, BEFORE_DISPATCH);
        assertFalse(result.isRouted());
    }

    @Test
    public void testTopLevelRoutePredicates_when_on() throws Exception {
        builder.setPredicate(RoutePredicates.alwaysTrue());

        builder.addRule("/avenger/Thor", "/secure/Thor.jspa", singletonList("GET"), RoutePredicates.alwaysTrue());

        builder.addRule(
                "/avenger/Thor/POSTorGET", "/secure/Thor.jspa", asList("GET", "POST"), RoutePredicates.alwaysTrue());

        routeRuleSet = builder.build();

        request = new MockHttpServletRequest("/avenger/Thor");

        UrlRouterImpl.Result result = router.route(request, BEFORE_DISPATCH);
        assertTrue(result.isRouted());

        request = new MockHttpServletRequest("/avenger/Thor/POSTorGET");

        result = router.route(request, BEFORE_DISPATCH);
        assertTrue(result.isRouted());
    }

    @Test
    public void testMultipleRuleSetsDoesJaxRSMatching() throws Exception {

        builder = newRouteSetBuilder();
        builder.addRule("/avenger/{character}/{id}/{weapon}", "/secure/LookIts{character}.jspa?withHis={weapon}");

        UrlRouteRuleSet routeRuleSet1 = builder.build();

        // in a single set world the former would over-ride this
        // but not when there are 2 sets
        builder = newRouteSetBuilder();
        builder.addRule("/avenger/{character}/{id}/{weapon}/{shield}", "/secure/LookItsAlso{character}.jspa");

        UrlRouteRuleSet routeRuleSet2 = builder.build();

        router = new UrlRouterImpl(null) {
            Set<UrlRouteRuleSet> getRouteRuleSets(FilterLocation filterLocation, String requestURI) {
                return new HashSet<>(asList(routeRuleSet1, routeRuleSet2));
            }
        };

        //
        // with multiple rule sets we use JAX-RS matching.

        request = new MockHttpServletRequest("/avenger/Thor/1/Hammer/Hair");

        UrlRouter.Result actual = router.route(request, BEFORE_DISPATCH);
        assertURLEquals("/secure/LookItsAlsoThor.jspa", actual);
        request = new MockHttpServletRequest("/avenger/Thor/1/Hammer/Hair");

        request = new MockHttpServletRequest("/avenger/Hulk/1/Fists");
        actual = router.route(request, BEFORE_DISPATCH);
        assertURLEquals("/secure/LookItsHulk.jspa?withHis=Fists", actual);
    }

    @Test
    @Parameters(method = "getParameters")
    public void testPredicateException(Level level, int expectedError, int expectedDebug) {
        RoutePredicate<UrlRouteRuleSet> predicate = (request, route) -> {
            throw new ArithmeticException();
        };
        builder.addRule("/foo", "/bar");
        builder.setPredicate(predicate);
        routeRuleSet = builder.build();
        request = new MockHttpServletRequest("/foo");

        startLogging(level);
        UrlRouter.Result actual = router.route(request, BEFORE_DISPATCH);

        assertFalse(actual.isRouted());
        assertEquals(memoryAppender.search(ERROR_MSG_PREDICATE, Level.ERROR).size(), expectedError);
        assertEquals(
                memoryAppender
                        .search(ArithmeticException.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError);
        assertEquals(memoryAppender.search(ERROR_MSG_PREDICATE, Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    @Test
    @Parameters(method = "getParameters")
    public void testPredicateLinkageError(Level level, int expectedError, int expectedDebug) {
        RoutePredicate<UrlRouteRuleSet> predicate = (request, route) -> {
            throw new LinkageError();
        };
        builder.addRule("/foo", "/bar");
        builder.setPredicate(predicate);
        routeRuleSet = builder.build();
        request = new MockHttpServletRequest("/foo");

        startLogging(level);
        UrlRouter.Result actual = router.route(request, BEFORE_DISPATCH);

        assertFalse(actual.isRouted());
        assertEquals(memoryAppender.search(ERROR_MSG_PREDICATE, Level.ERROR).size(), expectedError);
        assertEquals(
                memoryAppender
                        .search(LinkageError.class.getCanonicalName(), Level.ERROR)
                        .size(),
                expectedError);
        assertEquals(memoryAppender.search(ERROR_MSG_PREDICATE, Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    @Test(expected = OutOfMemoryError.class)
    public void testPredicateThrowable() {
        RoutePredicate<UrlRouteRuleSet> predicate = (request, route) -> {
            throw new OutOfMemoryError();
        };
        builder.addRule("/foo", "/bar");
        builder.setPredicate(predicate);
        routeRuleSet = builder.build();
        request = new MockHttpServletRequest("/foo");

        router.route(request, BEFORE_DISPATCH);
    }

    private Iterable getParameters() {
        return Arrays.asList(new Object[][] {
            // level, expectedError, expectedDebug
            {Level.ERROR, 1, 0},
            {Level.DEBUG, 1, 1}
        });
    }

    private void assertURLEquals(String expected, UrlRouter.Result result) {
        assertTrue(result.isRouted());

        URI expectedURI = UriBuilder.fromUri(expected).build();
        URI actualURI = UriBuilder.fromUri(result.toURI()).build();

        assertEquals(expectedURI.getPath(), actualURI.getPath());

        List<NameValuePair> expectedPairs = URLEncodedUtils.parse(expectedURI, "UTF-8");
        List<NameValuePair> actualPairs = URLEncodedUtils.parse(actualURI, "UTF-8");

        assertTrue(sameNVP(expectedPairs, actualPairs));
    }

    private boolean sameNVP(List<NameValuePair> expectedPairs, List<NameValuePair> actualPairs) {
        return containsNVPs(expectedPairs, actualPairs) && containsNVPs(expectedPairs, actualPairs);
    }

    private boolean containsNVPs(List<NameValuePair> expectedPairs, List<NameValuePair> actualPairs) {
        for (NameValuePair expectedPair : expectedPairs) {
            boolean containedWithin = false;
            for (NameValuePair actualPair : actualPairs) {
                if (NVP_COMPARATOR.compare(expectedPair, actualPair) == 0) {
                    containedWithin = true;
                    break;
                }
            }
            if (!containedWithin) {
                return false;
            }
        }
        return true;
    }

    public static final NVPComparator NVP_COMPARATOR = new NVPComparator();

    private static class NVPComparator implements Comparator<NameValuePair> {
        @Override
        public int compare(NameValuePair o1, NameValuePair o2) {
            int rc = o1.getName().compareTo(o2.getName());
            if (rc == 0) {
                rc = o1.getValue().compareTo(o2.getValue());
            }
            return rc;
        }
    }
}
