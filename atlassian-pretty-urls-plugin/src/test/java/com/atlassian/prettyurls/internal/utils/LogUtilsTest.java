package com.atlassian.prettyurls.internal.utils;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import com.atlassian.prettyurls.internal.route.MemoryAppender;
import com.atlassian.prettyurls.internal.util.LogUtils;

import static org.junit.Assert.assertEquals;

@RunWith(value = JUnitParamsRunner.class)
public class LogUtilsTest {

    private static final org.slf4j.Logger executionLogger = LoggerFactory.getLogger(LogUtilsTest.class);
    private Logger testLogger;
    private static MemoryAppender memoryAppender;
    private static final String LOGGER_NAME = "com.atlassian.prettyurls.internal.utils";
    private static final Exception EXCEPTION = new RuntimeException();
    private static final String MESSAGE = "Error message.";

    @Before
    public void setup() {
        testLogger = (Logger) LoggerFactory.getLogger(LOGGER_NAME);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
    }

    @After
    public void after() {
        memoryAppender.reset();
        memoryAppender.stop();
    }

    private void startLogging(Level level) {
        testLogger.setLevel(level);
        testLogger.addAppender(memoryAppender);
        memoryAppender.start();
    }

    @Test
    @Parameters(method = "getParameters")
    public void logExceptionEventWithParameters(
            Level level, Logger logger, Exception exception, String message, int expectedError, int expectedDebug) {
        startLogging(level);
        LogUtils.logExceptionEvent(logger, exception, message);

        assertEquals(memoryAppender.search(String.valueOf(message), Level.ERROR).size(), expectedError);
        assertEquals(memoryAppender.search(String.valueOf(message), Level.DEBUG).size(), expectedDebug);
        assertEquals(memoryAppender.countEventsForLogger(LOGGER_NAME), expectedError + expectedDebug);
    }

    private Iterable getParameters() {
        return Arrays.asList(new Object[][] {
            // level, logger, exception, message, expectedError, expectedDebug
            {Level.ERROR, executionLogger, EXCEPTION, MESSAGE, 1, 0},
            {Level.ERROR, null, EXCEPTION, MESSAGE, 0, 0},
            {Level.ERROR, executionLogger, null, MESSAGE, 1, 0},
            {Level.ERROR, executionLogger, EXCEPTION, null, 1, 0},
            {Level.ERROR, executionLogger, null, null, 1, 0},
            {Level.DEBUG, executionLogger, EXCEPTION, MESSAGE, 1, 1},
            {Level.DEBUG, null, EXCEPTION, MESSAGE, 0, 0},
            {Level.DEBUG, executionLogger, null, MESSAGE, 1, 1},
            {Level.DEBUG, executionLogger, EXCEPTION, null, 1, 1},
            {Level.DEBUG, executionLogger, null, null, 1, 1}
        });
    }
}
