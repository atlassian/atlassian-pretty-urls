package com.atlassian.prettyurls.internal.route;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.prettyurls.api.route.DefaultUrlRouteRuleSetKey;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(MockitoJUnitRunner.class)
public class RouteServiceImplTest {
    RouteServiceImpl routeService;

    @Mock
    PluginAccessor pluginAccessor;

    @Mock
    PluginEventManager pluginEventManager;

    @Before
    public void setUp() throws Exception {
        routeService = new RouteServiceImpl(pluginAccessor, pluginEventManager);
        routeService.afterPropertiesSet();
    }

    @Test
    public void testGetRoutes() throws Exception {
        Set<UrlRouteRuleSet> routes = routeService.getRoutes();
        assertThat(routes.isEmpty(), equalTo(true));

        routeService.registerRoutes(buildTestRuleSet("key1"));
        routeService.registerRoutes(buildTestRuleSet("key2"));
        routeService.registerRoutes(buildTestRuleSet("key3"));

        routes = routeService.getRoutes();
        assertThat(routes.isEmpty(), equalTo(false));
        assertThat(routes.size(), equalTo(3));
    }

    @Test
    public void testUnregisterRoutes() throws Exception {
        routeService.registerRoutes(buildTestRuleSet("key1"));
        routeService.registerRoutes(buildTestRuleSet("key2"));
        routeService.registerRoutes(buildTestRuleSet("key3"));

        Set<UrlRouteRuleSet> routes = routeService.getRoutes();
        assertThat(routes.size(), equalTo(3));

        UrlRouteRuleSet removed = routeService.unregisterRoutes(new DefaultUrlRouteRuleSetKey("key2"));
        assertThat(removed, notNullValue());

        routes = routeService.getRoutes();
        assertThat(routes.size(), equalTo(2));
    }

    private UrlRouteRuleSet buildTestRuleSet(String key) {
        return new UrlRouteRuleSet.Builder()
                .setKey(new DefaultUrlRouteRuleSetKey(key))
                .addTopLevelPath("/toplevel")
                .addRule("from", "to")
                .build();
    }
}
