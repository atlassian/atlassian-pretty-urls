package com.atlassian.prettyurls.internal.route;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.prettyurls.api.route.UrlRouteRule;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import static com.atlassian.prettyurls.internal.route.UrlMatcher.Strategy.JAX_RS_MATCHING;

public class UrlMatcherTest {

    private UrlMatcher classUnderTest;

    @Before
    public void setUp() throws Exception {
        classUnderTest = new UrlMatcher();
    }

    @Test
    public void test_multi_rule_set_ordering_is_in_jax_rs_order() throws Exception {

        List<UrlRouteRule> rules = standardRules();

        List<UrlRouteRule> actual = JAX_RS_MATCHING.sortRules(rules);

        List<UrlRouteRule> expected = asList(
                rule("/1/very/specific/non/variableZZ"),
                rule("/1/very/specific/non/variables"),
                rule("/10/this/is/less/specified/"),
                rule("/20/this/is/less/specific/"),
                rule("/30/this/is/less/spec/"),
                rule("/40/this/is/less/s/"),
                rule("/1000/vars/{withvar1}/{withvar2}/{withvar3}"),
                rule("/2000/vars/{withvar1}/{withvar2}"),
                rule("/3000/vars/{withvar1}"));

        assertEquals(expected, actual);
    }

    @Test
    public void test_multi_rule_set_matching_happens_as_expected() throws Exception {

        UrlMatcher.Result result;

        result = classUnderTest.getMatchingRule("", standardRules(), JAX_RS_MATCHING);
        assertThat(result.matches(), equalTo(false));

        result = classUnderTest.getMatchingRule("/1/very/specific/non/variableX", standardRules(), JAX_RS_MATCHING);
        assertThat(result.matches(), equalTo(false));

        result = classUnderTest.getMatchingRule("/1/very/specific/non/variableZZ", standardRules(), JAX_RS_MATCHING);
        assertThat(result.matches(), equalTo(true));

        result = classUnderTest.getMatchingRule("/2000/vars/v1/v2", standardRules(), JAX_RS_MATCHING);

        assertThat(result.matches(), equalTo(true));
        assertThat(result.getParsedVariableValues().get("withvar1"), equalTo("v1"));
        assertThat(result.getParsedVariableValues().get("withvar2"), equalTo("v2"));
    }

    @Test
    public void test_single_ruleset_in_legacy_list_order() throws Exception {
        /*
         * We need to not use JAX-RS ordering because of legacy compatibility.  Since we have
         * plenty of pretty url code out there that assumed "list" order then we must preserve it.
         *
         * So this means that capturing groups (aka variables) can precede explicit chars if they are
         * placed in list order first.  While not ideal (on reflection) is preserves backwards compatibility
         * for those that might have accidentally relied on it
         *
         * We only need this for "single rule sets" per top level path
         */

        List<UrlRouteRule> rules =
                asList(rule("/in/multi/set/this/would/be/{last}"), rule("/in/multi/set/this/would/be/first"));

        UrlMatcher.Result result = classUnderTest.getMatchingRule(
                "/in/multi/set/this/would/be/first", rules, UrlMatcher.Strategy.LIST_ORDER_MATCHING);

        assertThat(result.matches(), equalTo(true));
        assertThat(result.getParsedVariableValues().get("last"), equalTo("first"));
    }

    private List<UrlRouteRule> standardRules() {
        final List<UrlRouteRule> rules = new ArrayList<>();

        // when they are the same length with no variables they end up as a string comparison
        rules.addAll(shuffled("/1/very/specific/non/variableZZ", "/1/very/specific/non/variables"));

        rules.addAll(shuffled(
                "/40/this/is/less/s/",
                "/10/this/is/less/specified/",
                "/30/this/is/less/spec/",
                "/20/this/is/less/specific/"));

        rules.addAll(shuffled(
                "/2000/vars/{withvar1}/{withvar2}",
                "/1000/vars/{withvar1}/{withvar2}/{withvar3}",
                "/3000/vars/{withvar1}"));
        return rules;
    }

    /**
     * A shuffled set of rules truly proves that initial order does not matter
     *
     * @param path  the 1st path
     * @param paths more paths
     *
     * @return a shuffled list
     */
    private List<UrlRouteRule> shuffled(String path, String... paths) {
        List<UrlRouteRule> rules = rules(path, paths);
        Collections.shuffle(rules);
        return rules;
    }

    private List<UrlRouteRule> rules(String path, String... paths) {
        List<UrlRouteRule> rules = new ArrayList<>();
        rules.add(rule(path));
        for (String s : paths) {
            rules.add(rule(s));
        }
        return rules;
    }

    private UrlRouteRule rule(String path) {
        return new UrlRouteRule(path, "to", singletonList("GET"), UrlRouteRule.ParameterMode.PASS_ALL);
    }
}
