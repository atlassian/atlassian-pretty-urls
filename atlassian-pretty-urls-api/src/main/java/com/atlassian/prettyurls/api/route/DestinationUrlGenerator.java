package com.atlassian.prettyurls.api.route;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * A generator for the destination url that is being routed.
 */
public interface DestinationUrlGenerator {
    /**
     * Generate the destination url that is being routed.
     * @param httpRequest The HttpServletRequest object
     * @param variables The value of the template variables from the source url
     * @return The destination url
     */
    String generate(final HttpServletRequest httpRequest, final Map<String, String> variables);
}
