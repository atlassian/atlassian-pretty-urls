package com.atlassian.prettyurls.api.route;

import javax.servlet.http.HttpServletRequest;

/**
 * Simple helper methods for route predicates.  Clearly modelled off Guava Predicates
 *
 * @since 1.11.2
 */
public class RoutePredicates {
    enum ObjectPredicate implements RoutePredicate<Object> {
        ALWAYS_TRUE {
            @Override
            public boolean apply(final HttpServletRequest httpServletRequest, final Object route) {
                return true;
            }
        },
        ALWAYS_FALSE {
            @Override
            public boolean apply(final HttpServletRequest httpServletRequest, final Object route) {
                return false;
            }
        };

        @SuppressWarnings("unchecked")
        <T> RoutePredicate<T> withNarrowedType() {
            return (RoutePredicate<T>) this;
        }
    }

    public static <T> RoutePredicate<T> alwaysTrue() {
        return ObjectPredicate.ALWAYS_TRUE.withNarrowedType();
    }

    public static <T> RoutePredicate<T> alwaysFalse() {
        return ObjectPredicate.ALWAYS_FALSE.withNarrowedType();
    }
}
