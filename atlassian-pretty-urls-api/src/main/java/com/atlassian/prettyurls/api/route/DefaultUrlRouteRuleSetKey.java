package com.atlassian.prettyurls.api.route;

import static java.util.Objects.requireNonNull;

/**
 * A simple implementation based on a string
 *
 * @since 1.11.2
 */
public class DefaultUrlRouteRuleSetKey implements UrlRouteRuleSetKey {
    private final String key;

    public DefaultUrlRouteRuleSetKey(final String key) {
        this.key = requireNonNull(key);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DefaultUrlRouteRuleSetKey that = (DefaultUrlRouteRuleSetKey) o;

        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return "DefaultUrlRouteRuleSetKey{" + "key='" + key + '\'' + '}';
    }
}
