package com.atlassian.prettyurls.api.route;

import java.util.Set;

import com.atlassian.plugin.servlet.filter.FilterLocation;

/**
 * You can add dynamic routes via this service into the pretty URL system
 *
 * @since 1.11.2
 */
public interface RouteService {
    /**
     * @return the set of routes that have been registered, either dynamically or statically via plugin module
     * descriptors
     */
    Set<UrlRouteRuleSet> getRoutes();

    /**
     * Returns the rulesets that match the given {@link com.atlassian.plugin.servlet.filter.FilterLocation} and the requestURI
     *
     * @param filterLocation the filter location in play
     * @param requestURI     the request URI in play
     * @return the set of routes that match
     */
    Set<UrlRouteRuleSet> getRouteRuleSets(FilterLocation filterLocation, String requestURI);

    /**
     * Dynamically registers a route into the pretty URL system
     *
     * @param urlRouteRuleSet the rule set to register
     */
    void registerRoutes(UrlRouteRuleSet urlRouteRuleSet);

    /**
     * Dynamically un-registers a route from the pretty URL system
     *
     * @param key the rule set to un-register
     * @return the rule set that was removed
     */
    UrlRouteRuleSet unregisterRoutes(UrlRouteRuleSetKey key);
}
