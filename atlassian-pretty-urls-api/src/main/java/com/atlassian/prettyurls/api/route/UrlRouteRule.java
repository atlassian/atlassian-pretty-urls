package com.atlassian.prettyurls.api.route;

import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * Represents a from and to URI and hence something we can try to match to
 *
 * @since 1.11.2
 */
public class UrlRouteRule {
    /**
     * This controls how the variables inside the paths are passed on to redirected URLs
     */
    public enum ParameterMode {
        PASS_ALL,
        PASS_UNMAPPED,
        PASS_NONE
    }

    private final String from;
    private final String to;
    private final DestinationUrlGenerator toUriGenerator;
    private final ParameterMode parameterMode;
    private final List<String> httpVerbs;
    private final RoutePredicate<UrlRouteRule> predicate;

    public UrlRouteRule(
            final String from, final String to, final List<String> httpVerbs, final ParameterMode parameterMode) {
        this(from, to, httpVerbs, parameterMode, RoutePredicates.alwaysTrue());
    }

    public UrlRouteRule(
            final String from,
            final String to,
            final List<String> httpVerbs,
            final ParameterMode parameterMode,
            final RoutePredicate<UrlRouteRule> predicate) {
        this(from, to, null, httpVerbs, parameterMode, predicate);
    }

    public UrlRouteRule(
            final String from,
            final DestinationUrlGenerator toUriGenerator,
            final List<String> httpVerbs,
            final ParameterMode parameterMode) {
        this(from, toUriGenerator, httpVerbs, parameterMode, RoutePredicates.<UrlRouteRule>alwaysTrue());
    }

    public UrlRouteRule(
            final String from,
            final DestinationUrlGenerator toUriGenerator,
            final List<String> httpVerbs,
            final ParameterMode parameterMode,
            final RoutePredicate<UrlRouteRule> predicate) {
        this(from, null, toUriGenerator, httpVerbs, parameterMode, predicate);
    }

    private UrlRouteRule(
            final String from,
            final String to,
            final DestinationUrlGenerator toUriGenerator,
            final List<String> httpVerbs,
            final ParameterMode parameterMode,
            final RoutePredicate<UrlRouteRule> predicate) {
        this.from = requireNonNull(from);
        this.to = to;
        this.toUriGenerator = toUriGenerator;
        this.httpVerbs = requireNonNull(httpVerbs);
        this.parameterMode = requireNonNull(parameterMode);
        this.predicate = requireNonNull(predicate);
    }

    public String getFrom() {
        return from;
    }

    @Nullable
    public String getTo() {
        return to;
    }

    @Nullable
    public DestinationUrlGenerator getToUriGenerator() {
        return toUriGenerator;
    }

    public ParameterMode getParameterMode() {
        return parameterMode;
    }

    public List<String> getHttpVerbs() {
        return httpVerbs;
    }

    public RoutePredicate<UrlRouteRule> getPredicate() {
        return predicate;
    }

    @Override
    public String toString() {
        return "UrlRouteRule{" + "from="
                + from + ", to="
                + to + ", httpVerbs="
                + httpVerbs + ", parameterMode="
                + parameterMode + ", predicate="
                + predicate + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UrlRouteRule that = (UrlRouteRule) o;
        return Objects.equals(from, that.from)
                && Objects.equals(to, that.to)
                && parameterMode == that.parameterMode
                && Objects.equals(httpVerbs, that.httpVerbs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, parameterMode, httpVerbs);
    }
}
