package com.atlassian.prettyurls.api.route;

import javax.servlet.http.HttpServletRequest;

/**
 * This returns true if the route should be followed at this point in time.  This can be dynamic and will be called
 * every time a rule is executed
 *
 * @since 1.11.2
 */
public interface RoutePredicate<T> {
    /**
     * @return true if the route is in play
     */
    boolean apply(HttpServletRequest httpServletRequest, T route);
}
